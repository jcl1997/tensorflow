import * as tfvis from '@tensorflow/tfjs-vis';
import React, { useEffect, useState, Fragment } from "react";
import { createModel } from "./services/createModel";
import { convertToTensor } from "./services/convertToTensor";
import { trainModel } from "./services/trainModel";
import { testModel } from "./services/testModel";
import { dados } from "./dados";

export default function App() {
    const {
        data,
        model,
        loading: loadingTeste,
    } = useModel();

    const {
        loading: loadingTrainModel,
        count,
        setCount
    } = useTrainModel(data, model);

    const {
        loading: loadingTesteMode,
        count: countTesteMode,
        setCount: setCountTesteMode,
    } = useTesteMode(data, model);



    const loading = loadingTeste;

    return (
        <div>
            {loading
                ? (<Fragment>Carregando</Fragment>)
                : (
                    <Fragment>
                        <button onClick={() => setCount(count + 1)}>Treina</button>
                        <button onClick={() => setCountTesteMode(countTesteMode + 1)}>Teste</button>
                    </Fragment>
                )
            }
        </div>
    );
}

function useTesteMode(data, model) {
    const [loading, setLoading] = useState(false);
    const [count, setCount] = useState(0);

    useEffect(() => {
        if (data.length === 0) {
            return;
        }

        if (!model) {
            return;
        }

        async function execute() {
            setLoading(true);

            const tensorData = convertToTensor(data);

            // Train the model
            await testModel(model, data, tensorData);
            console.log("\x1b[34m%s\x1b[0m", 'Done testing');

            setLoading(false);
        }

        execute();
    }, [count]);

    return {
        loading,
        count,
        setCount
    }

}

function useTrainModel(data, model) {
    const [loading, setLoading] = useState(false);
    const [count, setCount] = useState(0);

    useEffect(() => {
        if (data.length === 0) {
            return;
        }

        if (!model) {
            return;
        }

        async function execute() {
            setLoading(true);

            const tensorData = convertToTensor(data);
            const {inputs, labels} = tensorData;

            // Train the model
            await trainModel(model, inputs, labels);
            console.log("\x1b[34m%s\x1b[0m", 'Done Training');

            setLoading(false);
        }

        execute();
    }, [count]);

    return {
        loading,
        count,
        setCount
    }
}

function useModel() {
    const [data, setData] = useState([]);
    const [model, setModel] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function execute() {
            // TODO: carregas do dados e formatar
            const items = dados.map(item => ({
                horsepower: item.Horsepower,
                mpg: item.Miles_per_Gallon,
            }))
            .filter(car => car.horsepower != null && car.mpg != null);

            // TODO: Gráfico de dispersão de dados
            tfvis
                .render
                .scatterplot(
                    { name: 'Horsepower v MPG' },
                    {
                        values: items.map(item => ({
                            x: item.horsepower,
                            y: item.mpg,
                        }))
                    },
                    {
                        xLabel: 'Horsepower',
                        yLabel: 'MPG',
                        height: 300
                    }
                );

            const currentModel = createModel();

            tfvis
                .show
                .modelSummary(
                    { name: 'Model Summary' }, currentModel
                );

            setModel(currentModel);
            setData(items);
            setLoading(false);
        }

        execute();
    }, []);

    return {
        data,
        model,
        loading
    };
}
