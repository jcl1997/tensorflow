import * as tf from '@tensorflow/tfjs';

export function createModel() {
    // TODO: Arquitetura do modelo
    // TODO: Como o modelo vai calcular as respostas

    const model = tf.sequential();

    // TODO: Modelo ML  uma entra e uma saída

    // TODO: Camada de entrada
    // TODO: Dense multiplica as entrada pela matriz de pesos
    // TODO: Units tamanho da matriz de pesos
    model.add(tf.layers.dense({
        inputShape: [1],
        units: 1,
        activation: 'sigmoid',
        useBias: true,
    }));

    // TODO: Camada de Saída
    model.add(tf.layers.dense({
        inputShape: [1],
        units: 1,
        activation: 'sigmoid',
        useBias: true,
    }));

    return model;
}
