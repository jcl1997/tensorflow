import * as tf from '@tensorflow/tfjs';

export function convertToTensor(data) {
  // TODO: Organiza os dados e remove valore intermediários

  return tf.tidy(() => {
    // TODO: Reproduz os dados de forma aleatória
    tf.util.shuffle(data);

    // TODO: converte os dados
    const inputs = data.map(d => d.horsepower)
    const labels = data.map(d => d.mpg);

    const inputTensor = tf.tensor2d(inputs, [inputs.length, 1]);
    const labelTensor = tf.tensor2d(labels, [labels.length, 1]);

    // TODO: Normaliza os dados
    // TODO: Importante para evitar problemas na aprendizagem e tonar mais eficaz
    const inputMax = inputTensor.max();
    const inputMin = inputTensor.min();
    const labelMax = labelTensor.max();
    const labelMin = labelTensor.min();

    const normalizedInputs = inputTensor.sub(inputMin).div(inputMax.sub(inputMin));
    const normalizedLabels = labelTensor.sub(labelMin).div(labelMax.sub(labelMin));

    return {
      inputs: normalizedInputs,
      labels: normalizedLabels,
      inputMax,
      inputMin,
      labelMax,
      labelMin,
    }
  });
}