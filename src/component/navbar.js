import React from 'react';

export default function navbar() {  
    return (
        <div className=" w-100 vh-30 bg-primary">
            <div className="vh-auto wh-auto m-3">
                <div className="
                    text-white
                    text-uppercase
                    text-center
                    vertical-center"
                >
                    REDE NEURAL
                </div>
            </div>
        </div>
    );
}
