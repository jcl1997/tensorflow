import React, { useState } from 'react';
import Sidebar from './sidebar';
import NavBar from './navbar';
import Menu from './menu';
import Router from './router';

export default function App() {
    const [show, setShow] = useState(false);
    return (
        <div className="container-fluid">
            <div className="row">
                {show 
                    ? (
                        <div className="col-2 bg-primary vh-100">
                            <Sidebar
                                show={show}
                                setShow={setShow}
                            />
                        </div>
                    )
                    : (
                        <div className="col-auto bg-primary vh-100">
                            <Menu
                                show={show}
                                setShow={setShow}
                            />
                        </div>
                    )
                }
                <div className="col vh-100">
                    <div className="row">
                        <NavBar />
                    </div>
                    <div className="row">
                        <Router />
                    </div>
                </div>
            </div>
        </div>
    );
}