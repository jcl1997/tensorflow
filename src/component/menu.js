import React from 'react';
import { Link } from "react-router-dom";

export default function menu({
    setShow,
    show,
}) {
    return (
        <div className="p-0 m-1">
            <div className="list-group">
                <div className="
                    list-group-item
                    list-group-item-action
                    text-center
                    text-white
                    bg-primary"
                    title="Abrir menu"
                    onClick={() => setShow(!show)}
                >
                    <i
                        className="fa fa-bars"
                        size={25}
                    />
                </div>
            </div>
            <hr />
            <div className="list-group">
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-center
                        text-white
                        bg-primary"
                    to="/"
                    title="Home"
                >
                    <i className="fa fa-home" />
                </Link>
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-center
                        text-white
                        bg-primary"
                    to="/Profile"
                    title="Profile"
                >
                    <i className="fa fa-user" />
                </Link>
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-center
                        text-white
                        bg-primary"
                    to="/Messages"
                    title="Messages"
                >
                    <i className="fa fa-envelope" />
                </Link>
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-center
                        text-white
                        bg-primary"
                    to="/Settings"
                    title="Settings"
                >
                    <i className="fa fa-cog" />
                </Link>
            </div>
            <hr />
        </div>
    );
}
