import React from 'react';
import { Link } from "react-router-dom";

export default function sidebar({
    setShow,
    show,
}) {
    return (
        <div>
            <div className="
                vh-auto
                wh-auto
                m-3
                text-white
                text-uppercase
                text-center
                vertical-center"
            >
                <i
                    title="Fechar menu"
                    className="fa fa-bars"
                    size={25}
                    onClick={() => setShow(!show)}
                />
                &nbsp;
                REDE NEURAL
            </div>
            <hr />
            <div className="list-group list-group-flush">
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-white
                        bg-primary"
                    to="/"
                    title="Home"
                >
                    <i className="fa fa-home" />
                    &nbsp;
                    Home
                </Link>
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-white
                        bg-primary"
                    to="/Profile"
                    title="Profile"
                >
                    <i className="fa fa-user" />
                    &nbsp;
                    Profile
                </Link>
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-white
                        bg-primary"
                    to="/Messages"
                    title="Messages"
                >
                    <i className="fa fa-envelope" />
                    &nbsp;
                    Messages
                </Link>
                <Link
                    className="
                        list-group-item
                        list-group-item-action
                        text-white
                        bg-primary"
                    to="/Settings"
                    title="Settings"
                >
                    <i className="fa fa-cog" />
                    &nbsp;
                    Settings
                </Link>
            </div>
            <hr />
        </div>
    );
}
