import React from "react";
import {
  Switch,
  Route,
} from "react-router-dom";
import Home from './home'
import Profile from './profile'
import Messages from './messages';
import Settings from './settings';

export default function App() {
  return (
        <Switch>
            <Route path="/Settings">
                <Settings />
            </Route>
            <Route path="/Messages">
                <Messages />
            </Route>
            <Route path="/Profile">
                <Profile />
            </Route>
            <Route path="/">
                <Home />
            </Route>
        </Switch>
  );
}